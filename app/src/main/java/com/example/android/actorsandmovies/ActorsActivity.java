package com.example.android.actorsandmovies;

import android.content.Context;
import android.content.Intent;
import android.provider.SyncStateContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ActorsActivity extends AppCompatActivity implements ActorsAdapter.OnItemClickListener{

    List<Actor> actors;
    RecyclerView recyclerView;
    ActorsAdapter adapter;
    LinearLayoutManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       // actors = parse();
        actors = new ArrayList<>();
        String json = loadJSONFromAsset();
        Type listType = new TypeToken<ArrayList<Actor>>() {}.getType();
        actors = (ArrayList<Actor>) ConvertToObject(json, listType);
        recyclerView = (RecyclerView) findViewById(R.id.rview);
        adapter = new ActorsAdapter(ActorsActivity.this, actors, this);
        recyclerView.setAdapter(adapter);
        manager = new LinearLayoutManager(ActorsActivity.this);
        recyclerView.setLayoutManager(manager);

    }


    public String loadJSONFromAsset() {
        String json = "";
        try {

            InputStream is = getAssets().open("test.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer);


        } catch (IOException ex) {}
        return json;
    }


   public static <T> T ConvertToObject(String json, Type type) {
        Gson gson = new Gson();
        return gson.fromJson(json, type);
    }
/*
    public List<Actor> parse() {
        List<Actor> actors = new ArrayList<>();
        String json = loadJSONFromAsset();
        JsonParser parser = new JsonParser();
        JsonArray array = parser.parse(json).getAsJsonArray();

        for (int i = 0; i < array.size(); ++i) {
            JsonObject object = array.get(i).getAsJsonObject();
            Actor actor = new Actor();
            actor.setName(object.get("FullName").getAsString());
            actor.setDOB(object.get("DOB").getAsString());
            actor.setPicture(object.get("photoID").getAsString());

            actors.add(actor);
        }

        return  actors;
    }
    */

    @Override
    public void OnItemClick(Actor actor) {
        Intent intent = new Intent(this, ActorAndMoviesActivity.class);
        intent.putExtra("actor", actor);
        startActivity(intent);
    }
}