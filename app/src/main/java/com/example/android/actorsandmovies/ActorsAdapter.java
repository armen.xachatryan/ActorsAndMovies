package com.example.android.actorsandmovies;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Armen on 11.08.2017.
 */

public class ActorsAdapter extends RecyclerView.Adapter<ActorsAdapter.ViewHolder> {

    public interface OnItemClickListener {
         void OnItemClick(Actor actor);
    }

    List<Actor> list;
    OnItemClickListener listener;
    LayoutInflater inflater;
    Context context;

    public ActorsAdapter(Context context, List<Actor> list, OnItemClickListener listener) {

        this.context = context;
        inflater = LayoutInflater.from(context);
        this.list = list;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.actors, null);
        ViewHolder rviewholder = new ViewHolder(view, list, context);
        return rviewholder;
    }


    public void onBindViewHolder(ViewHolder holder,final int position) {
        holder.bind(list.get(position), listener);

    }

    @Override
    public int getItemCount() {

        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public Button name;
        public TextView DOB;
        public TextView POB;
        public ImageView picture;
        public TextView awards;
        public ImageView poster;
        public Button title;
        public ImageView poster1;
        public Button title1;
        public ImageView poster2;
        public Button title2;
        public ImageView poster3;
        public Button title3;
        public ImageView poster4;
        public Button title4;
        public ImageView poster5;
        public Button title5;

        List<Actor> actors = new ArrayList<>();
        Context context;

        public ViewHolder(View view, List<Actor> actors, Context context) {
            super(view);
            this.actors = actors;
            this.context = context;
            name = (Button) view.findViewById(R.id.ActorName);
            DOB = (TextView) view.findViewById(R.id.birthDate);
            POB = (TextView) view.findViewById(R.id.pob);
            awards = (TextView) view.findViewById(R.id.awards);
            picture = (ImageView) view.findViewById(R.id.picture);

            poster = (ImageView) view.findViewById(R.id.poster);
            title = (Button) view.findViewById(R.id.title);
            poster1 = (ImageView) view.findViewById(R.id.poster1);
            title1 = (Button) view.findViewById(R.id.title1);
            poster2 = (ImageView) view.findViewById(R.id.poster2);
            title2 = (Button) view.findViewById(R.id.title2);
            poster3 = (ImageView) view.findViewById(R.id.poster3);
            title3 = (Button) view.findViewById(R.id.title3);
            poster4 = (ImageView) view.findViewById(R.id.poster4);
            title4 = (Button) view.findViewById(R.id.title4);
            poster5 = (ImageView) view.findViewById(R.id.poster5);
            title5 = (Button) view.findViewById(R.id.title5);

        }

        public void bind(final Actor actor, final OnItemClickListener listener) {
            name.setText(actor.getName());
            DOB.setText(actor.getDOB());
            int resourceID = context.getResources().getIdentifier(actor.getPicture(), "drawable", context.getPackageName());
            picture.setImageResource(resourceID);

            name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.OnItemClick(actor);

                }
            });

        }
    }
}
