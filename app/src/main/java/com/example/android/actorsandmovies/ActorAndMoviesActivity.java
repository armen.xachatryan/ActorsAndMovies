package com.example.android.actorsandmovies;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Armen on 23.08.2017.
 */

public class ActorAndMoviesActivity extends AppCompatActivity implements ActorAndMoviesAdapter.OnItemClickListener{

    List<Actor> actors;
    RecyclerView recyclerView;
    LinearLayoutManager manager;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        Actor actor = (Actor) intent.getSerializableExtra("actor");
        recyclerView = (RecyclerView) findViewById(R.id.rview);
        ActorAndMoviesAdapter adapter = new ActorAndMoviesAdapter(ActorAndMoviesActivity.this, actor, this);
        recyclerView.setAdapter(adapter);
        manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);

    }

    @Override
    public void OnClick(Movie movie) {
        Intent intent = new Intent(ActorAndMoviesActivity.this, MovieActicity.class);
        intent.putExtra("movie", movie);
        startActivity(intent);
    }
}
