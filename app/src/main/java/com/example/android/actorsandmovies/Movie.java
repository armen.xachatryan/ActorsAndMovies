package com.example.android.actorsandmovies;

import android.widget.ImageView;

import java.io.Serializable;

/**
 * Created by Armen on 11.08.2017.
 */

public class Movie implements Serializable{

     String title;
     String plot;
     String director;
     String releaseDate;
     String genre;
     String posterPicture;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) { this.plot = plot; }

    public String getGenre() { return genre; }

    public void setGenre(String genre) { this.genre = genre; }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getPosterPicture() {
        return posterPicture;
    }

    public void setPosterPicture(String posterPicture) {
        this.posterPicture = posterPicture;
    }
}
