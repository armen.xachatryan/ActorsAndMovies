package com.example.android.actorsandmovies;

import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.net.URL;
import java.util.List;

/**
 * Created by Armen on 11.08.2017.
 */

public class Actor implements Serializable{

    private String fullname;
    private String DOB;
    private String POB;
    private String awards;
    private String picture;
    private List<Movie> movies;

    public List<Movie> getMovies() { return movies; }

    public void setMovies(List<Movie> movies) { this.movies = movies; }

    public String getName() {
        return fullname;
    }

    public void setName(String fullname) {
        this.fullname = fullname;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getPOB() {
        return POB;
    }

    public void setPOB(String POB) {
        this.POB = POB;
    }

    public String getAwards() { return awards; }

    public void setAwards(String awards) {
        this.awards = awards;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

}
