package com.example.android.actorsandmovies;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MovieActicity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_activity);

        TextView title = (TextView) findViewById(R.id.title);
        ImageView poster= (ImageView) findViewById(R.id.poster);
        TextView releaseDate = (TextView) findViewById(R.id.releaseDate);
        TextView director = (TextView) findViewById(R.id.director);
        TextView genre = (TextView) findViewById(R.id.genre);
        TextView plot = (TextView) findViewById(R.id.plot);

        Intent intent = getIntent();
        Movie movie = (Movie) intent.getSerializableExtra("movie");

        title.setText(movie.getTitle());
        int posterID = this.getResources().getIdentifier(movie.getPosterPicture(), "drawable", this.getPackageName());
        poster.setImageResource(posterID);
        releaseDate.setText(movie.getReleaseDate());
        director.setText("Director: " + movie.getDirector());
        genre.setText("Genre: " + movie.getGenre());
        plot.setText(movie.getPlot());

    }
}
