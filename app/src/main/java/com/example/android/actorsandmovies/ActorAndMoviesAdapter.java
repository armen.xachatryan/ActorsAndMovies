package com.example.android.actorsandmovies;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Armen on 23.08.2017.
 */

public class ActorAndMoviesAdapter extends RecyclerView.Adapter<ActorAndMoviesAdapter.ViewHolder> {

    Actor actor;
    LayoutInflater inflater;
    Context context;
    OnItemClickListener listener;

    public interface OnItemClickListener {
        public void OnClick(Movie movie);
    }

    public ActorAndMoviesAdapter(Context context, Actor actor, OnItemClickListener listener) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.actor = actor;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.actorsandmovies, null);
        ViewHolder viewholder = new ActorAndMoviesAdapter.ViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.bind(actor, listener);
    }

    @Override
    public int getItemCount() {

        return 1;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView picture;
        public TextView awards;
        public TextView POB;
        public TextView name;
        public TextView DOB;
        public ImageView poster;
        public Button title;
        public ImageView poster1;
        public Button title1;
        public ImageView poster2;
        public Button title2;
        public ImageView poster3;
        public Button title3;
        public ImageView poster4;
        public Button title4;
        public ImageView poster5;
        public Button title5;


        public ViewHolder(View view) {
             super(view);
             picture = (ImageView) view.findViewById(R.id.image);
             name = (TextView) view.findViewById(R.id.Name);
             DOB = (TextView) view.findViewById(R.id.dob);
             POB = (TextView) view.findViewById(R.id.pob);
             awards = (TextView) view.findViewById(R.id.awards);
             title = (Button) view.findViewById(R.id.title);
             poster = (ImageView) view.findViewById(R.id.poster);
             title1 = (Button) view.findViewById(R.id.title1);
             poster1 = (ImageView) view.findViewById(R.id.poster1);
             title2 = (Button) view.findViewById(R.id.title2);
             poster2 = (ImageView) view.findViewById(R.id.poster2);
             title3 = (Button) view.findViewById(R.id.title3);
             poster3 = (ImageView) view.findViewById(R.id.poster3);
             title4 = (Button) view.findViewById(R.id.title4);
             poster4 = (ImageView) view.findViewById(R.id.poster4);
             title5 = (Button) view.findViewById(R.id.title5);
             poster5 = (ImageView) view.findViewById(R.id.poster5);

        }

        public void bind(final Actor actor, final OnItemClickListener listener) {


            int pictureID = context.getResources().getIdentifier(actor.getPicture(), "drawable", context.getPackageName());
            picture.setImageResource(pictureID);
            awards.setText("Awards: " + actor.getAwards());
            POB.setText(actor.getPOB());
            name.setText(actor.getName());
            DOB.setText("Born: " + actor.getDOB()+ ", in");

            title.setText(actor.getMovies().get(0).getTitle());
            int posterID = context.getResources().getIdentifier(actor.getMovies().get(0).getPosterPicture(), "drawable", context.getPackageName());
            poster.setImageResource(posterID);

            title1.setText(actor.getMovies().get(1).getTitle());
            int poster1ID = context.getResources().getIdentifier(actor.getMovies().get(1).getPosterPicture(), "drawable", context.getPackageName());
            poster1.setImageResource(poster1ID);

            title2.setText(actor.getMovies().get(2).getTitle());
            int poster2ID = context.getResources().getIdentifier(actor.getMovies().get(2).getPosterPicture(), "drawable", context.getPackageName());
            poster2.setImageResource(poster2ID);

            title3.setText(actor.getMovies().get(3).getTitle());
            int poster3ID = context.getResources().getIdentifier(actor.getMovies().get(3).getPosterPicture(), "drawable", context.getPackageName());
            poster3.setImageResource(poster3ID);


            title4.setText(actor.getMovies().get(4).getTitle());
            int poster4ID = context.getResources().getIdentifier(actor.getMovies().get(4).getPosterPicture(), "drawable", context.getPackageName());
            poster4.setImageResource(poster4ID);

            title5.setText(actor.getMovies().get(5).getTitle());
            int poster5ID = context.getResources().getIdentifier(actor.getMovies().get(5).getPosterPicture(), "drawable", context.getPackageName());
            poster5.setImageResource(poster5ID);

            title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnClick(actor.getMovies().get(0));
                }
            });

            title1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnClick(actor.getMovies().get(1));
                }
            });
            title2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnClick(actor.getMovies().get(2));
                }
            });
            title3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnClick(actor.getMovies().get(3));
                }
            });
            title4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnClick(actor.getMovies().get(4));
                }
            });
            title5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnClick(actor.getMovies().get(5));
                }
            });
        }
    }
}
